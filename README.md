![A Gif of a scene from the movie: Star Wars Epsiode 3: Revenge of the Sith. In this scene, the jedi Obi-Wan Kenobi pops up in the frame and says the words 'Hello There.'](https://gitlab.com/dpopkin/dpopkin/-/raw/main/generalKenobi.gif){width="100%" height=50%}
<div align="center">

Source: <a href="https://giphy.com/gifs/starwars-star-wars-episode-3-xTiIzJSKB4l7xTouE8" target="_blank">Giphy</a></div>

<hr>
<p>Daniel Popkin here! A guy with a traditional education, +6 years of professional work and an interest in infosec.</p>

<p>My professional work has taken me across various finance sectors, including: retirement planning software, Real Estate, and more. Almost all of this revolves around a traditional L[A/N]MP stack with Laravel playing a huge role in that. With a few years of Ruby on Rails included in that as well.</p>

<p>Personal has gone to different sectors as well. Most notably is the, pardon my language, <a href="https://gitlab.com/dpopkin/dvla" target="_blank">Damn Vulnerable Laravel App</a>. An "insecure-by-design" PHP web app using the Laravel framework that can be used for security training. I add challenges when I can and as of right now, I'm researching a CSP bypass challenge. I also plan to add an easy way to get answers if you get stuck. Perhaps a video guide?</p>

<p>"That's all well and good, but what about something practical?" I hear you ask. Well, how about a local command line <a href="https://gitlab.com/dpopkin/subscription-manager" target="_blank">subscription manager</a> for the video streaming site YouTube? Taking advantage of YouTubes semi-hidden (but completely public) RSS feeds for channels (EX: https://www.youtube.com/feeds/videos.xml?channel_id=UC6Om9kAkl32dWlDSNlDS9Iw is the RSS video feed of the Defcon Conference). "But that's command line!" I hear you say. Except all you'd need to do is download the project, import your subscriptions from YouTube, put them in the same directory and run. My program does all the work. It also does much more, but I'll let the repo do the rest of the talking.</p>

<p>I, like every developer, do some blogging. Which can be found <a href="https://dpopkin.gitlab.io/" target="_blank">here</a>. I focus mostly on technology but also have a large <a href="https://dpopkin.gitlab.io/other/2023/06/07/be-more-secure.html" target="_blank"> online privacy/security guide</a> I update every so often.

<p>I suppose I should mention hobbies here as well. Outside of tech, I am an avid runner and brew Kombucha on the side.</p>

<p>If you made it all the way here, thanks for taking the time to read all this!</p>
